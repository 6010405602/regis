package sample;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;


import javax.swing.event.ChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    TableView<Subject> table_view;
    @FXML
    TableColumn<Subject,String> col_ID,col_subject,col_credit,col_level;
    @FXML
    TableColumn col_check;
    @FXML
    TextArea show_all;
    @FXML
    ChoiceBox termSelector;
    @FXML
    Button selectButton;
    @FXML Button show_de;
    @FXML TextField idTextField,nameTextField,creditTextField,levelTextField,basicSubTextField;


    SubjectList subjectList = new SubjectList();




    /*public void setup() {

        PseudoClass hard = PseudoClass.getPseudoClass("hard");
        PseudoClass easy = PseudoClass.getPseudoClass("easy");
        PseudoClass normal = PseudoClass.getPseudoClass("normal");




//Set a rowFactory for the table view.

        table_view.setRowFactory(tableView -> {
            TableRow<Subject> row = new TableRow<>();
            TableColumn<Subject, String> column = new TableColumn<>();


            row.itemProperty().addListener((obs, previousStock, currentStock) -> {

                if (currentStock != null) {
                    currentStock.getLevel();
                    row.pseudoClassStateChanged(hard, currentStock.getLevel() == "3" );
                    row.pseudoClassStateChanged(normal, currentStock.getLevel() == "2");
                    row.pseudoClassStateChanged(easy, currentStock.getLevel() == "1");
                } else {

                    row.pseudoClassStateChanged(hard, false);
                    row.pseudoClassStateChanged(normal, false);
                    row.pseudoClassStateChanged(easy, false);
                }
            });
            return row;
        });
    }
*/
    public ObservableList<Subject> getSubjectAll(){

        //setup();

        ArrayList<Subject> subAll = new ArrayList<>();
        subAll = subjectList.getSubAll();
        ObservableList<Subject> subjects = FXCollections.observableArrayList(subAll);

        return subjects;
    }

    public ObservableList<Subject> getSubjectTerm(int term){
        ArrayList<Subject> sublist  = subjectList.getSublectOfTerm(term);
        ObservableList<Subject> subjects = FXCollections.observableArrayList(sublist);
        return subjects;
    }





    @Override
    public void initialize(URL location, ResourceBundle resources) {
        col_ID.setCellValueFactory(new PropertyValueFactory<Subject, String>("subID"));
        col_subject.setCellValueFactory(new PropertyValueFactory<Subject, String>("subName"));
        col_credit.setCellValueFactory(new PropertyValueFactory<Subject, String>("credit"));
        col_level.setCellValueFactory(new PropertyValueFactory<Subject, String>("level"));

        col_check.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Subject, Boolean>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Subject, Boolean> param) {
                return param.getValue().getIsLeaned();
            }
        });
        col_check.setCellFactory(CheckBoxTableCell.forTableColumn(col_check));

        table_view.setItems(getSubjectAll());

        table_view.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            Subject subject = newValue;
            idTextField.setText(subject.getSubID());
            nameTextField.setText(subject.getSubName());
            creditTextField.setText(subject.getCredit());
            basicSubTextField.setText(subject.getSubBasic());
            levelTextField.setText(subject.getLevel());

        });

        termSelector.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->{
            int key = (Integer) newValue;
            if(key == 0){
                table_view.setItems(getSubjectAll());
            }
            else {
                table_view.setItems(getSubjectTerm(key));
            }
        });


        termSelector.getItems().addAll("All","Year 1 Term 1","Year 1 Term 2","Year 2 Term 1","Year 2 Term 2","Year 3 Term 1","Year 3 Term 2","Year 4 Term 1","Year 4 Term 2");
        termSelector.setValue("All");

        col_level.setCellFactory(tableColumn -> {
            return new TableCell<Subject,String>() {
                @Override
                protected void updateItem(final String item,final boolean empty){
                    super.updateItem(item,empty);

                    if(empty||item == null){
                        setText(null);
                        setGraphic(null);
                        setStyle(null);
                    }else {

                        if(item.equals("3")){
                            setStyle(" -fx-background-color: #ce1e1e;");
                        }
                        else if(item.equals("2")){
                            setStyle("-fx-background-color: #56aaff;");
                        }
                        else if(item.equals("1")){
                            setStyle("-fx-background-color: #a1d640;");
                        }
                    }
                }
            };
        });

    }

    public void handleClickToShow(javafx.event.ActionEvent actionEvent) {
        ObservableList<Subject> allSub, selected;
        allSub = table_view.getItems();
        selected = table_view.getSelectionModel().getSelectedItems();

        for(Subject sub : selected){
            String s = sub.getSubName();
            String sh = subjectList.getNewDetails(s);
            show_all.setText(sh);

        }

    }

    public void selectButtonOnActionhandle(){
        System.out.println(termSelector.getSelectionModel().getSelectedIndex());
        String str = (String) termSelector.getValue();
        int term=0;
        if(!str.equals("All")) {
            if (str.equals("Year 1 Term 1")) {
                term = 1;
            } else if (str.equals("Year 1 Term 2")) {
                term = 2;
            } else if (str.equals("Year 2 Term 1")) {
                term = 3;
            } else if (str.equals("Year 2 Term 2")) {
                term = 4;
            } else if (str.equals("Year 3 Term 1")) {
                term = 5;
            } else if (str.equals("Year 3 Term 2")) {
                term = 6;
            } else if (str.equals("Year 4 Term 1")) {
                term = 7;
            } else if (str.equals("Year 4 Term 2")) {
                term = 8;
            }
            table_view.setItems(getSubjectTerm(term));
        }else {
            table_view.setItems(getSubjectAll());
        }

    }

}
