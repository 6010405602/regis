package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Register.fxml"));
        Scene scene = new Scene(root, 900,600);
        scene.getStylesheets().add(getClass().getResource("Page.css").toExternalForm());
        primaryStage.setTitle("Register");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
//        SubjectList s =new SubjectList();
//        ArrayList<Subject> subAll = new ArrayList<>();
//        subAll = s.getSubAll();
//        System.out.println(subAll.get(0).getSubName());
//
//        System.out.println(s.getDetail(1,2));



    }
}
