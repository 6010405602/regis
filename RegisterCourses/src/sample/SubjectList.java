package sample;

import java.util.ArrayList;
import java.util.TreeMap;

public class SubjectList {

    private ArrayList<Subject> term1;
    private ArrayList<Subject> term2;
    private ArrayList<Subject> term3;
    private ArrayList<Subject> term4;
    private ArrayList<Subject> term5;
    private ArrayList<Subject> term6;
    private ArrayList<Subject> term7;
    private ArrayList<Subject> term8;
    private TreeMap<Integer, ArrayList<Subject>> allSubject;


    public SubjectList(){


        this.allSubject = new TreeMap<>();
        this.term1 = new ArrayList<>();
        this.term2 = new ArrayList<>();
        this.term3 = new ArrayList<>();
        this.term4 = new ArrayList<>();
        this.term5 = new ArrayList<>();
        this.term6 = new ArrayList<>();
        this.term7 = new ArrayList<>();
        this.term8 = new ArrayList<>();

        //term 1
        Subject cal_1 = new Subject("Calculus 1","01417111","3","no","2");
        term1.add(cal_1);

        Subject funPro = new Subject("Fund Programming Concepts","01418112","3","no","2");
        term1.add(funPro);

        Subject intro = new Subject("Introduction to Computer Science","01418114","2","no","1");
        term1.add(intro);

        Subject digi = new Subject("Digital Computer Logic","01418131","3","no","2");
        term1.add(digi);

        Subject Knowledge  = new Subject("Knowledge of the Land","01999111","2","no","1");
        term1.add(Knowledge);

        allSubject.put( 1, term1);

        //term2
        Subject cal_2 = new Subject("Calculus 2","01417112","3","Calculus 1","3");
        term2.add(cal_2);

        Subject comPro = new Subject("Computer Programming","01418113","3","Fund Programming Concepts","3");
        term2.add(comPro);

        Subject funCom = new Subject("Fundamental of Computing","01418113","4","no","3");
        term2.add(funCom);

        allSubject.put(2 , term2);

        //term3
        Subject linear = new Subject("Introductory Linear Algebra","01417322","3","Calculus 2","3");
        term3.add(linear);

        Subject soft = new Subject("Software Construction","01418211","3","Computer Programming","3");
        term3.add(soft);

        Subject data = new Subject("Data Structure","01418231","3","Computer Programming","3");
        term3.add(data);

        Subject stat = new Subject("Principles of  Statistics ","01422111","3","no","3");
        term3.add(stat);

        allSubject.put(3, term3);

        //term4
        Subject funData = new Subject("Fundamental of Database Systems","01418221","3","no","3");
        term4.add(funData);

        Subject algor = new Subject("Algorithms Design and Analysis","01418232","3","no","3");
        term4.add(algor);

        Subject assem = new Subject("Assembly Lang and Comp. Arch","01418233","4","no","3");
        term4.add(assem);

        allSubject.put(4 , term4);

        //term5
        Subject system = new Subject("System Analysis and Design","01418321","3","Algorithms Design and Analysis","3");
        term5.add(system);

        Subject operating = new Subject("Operating Systems","01418331","4","no","3");
        term5.add(operating);

        Subject intel = new Subject("Intellectual Prop. & Profess Ethics","01418341","3","no","3");
        term5.add(intel);

        Subject semi = new Subject("Seminar","01418497","1","no","3");
        term5.add(semi);

        allSubject.put(5 , term5);

        //term6
        Subject secur = new Subject("Information System Security","01418332","3","no","3");
        term6.add(secur);

        Subject auto = new Subject("Automata Theory","01418333","2","no","3");
        term6.add(auto);

        Subject compiler = new Subject("Compiler Techniques","01418334","2","no","3");
        term6.add(compiler);

        Subject ccs = new Subject("Prin. in CCs & Cloud Computing","01418351","3","no","3");
        term6.add(ccs);

        Subject co_op = new Subject("Co-op Education Preparation","01418390","1","no","3");
        term6.add(co_op);
        allSubject.put(6 , term6);


        //term7
        Subject co_op_edu = new Subject("Co-op Education","01418490","6","no","3");
        term7.add(co_op_edu);
        allSubject.put(7, term7);

        //term8
        Subject cs_pro = new Subject("CS Project","01418499","3","no","3");
        term8.add(cs_pro);
        allSubject.put(8, term8);
    }


    public String getS(int t){
        String sts =  "";
        int n = 0;
        if(allSubject.containsKey(t)){
            n = allSubject.get(t).size();
            for(int i = 0; i < n;i++){

                sts += allSubject.get(t).get(i).getSubName() + "\n" +
                        allSubject.get(t).get(i).getCredit() + "\n";
            }
        }
        return sts;
    }

    public ArrayList<Subject> getSublectOfTerm(int term){
        ArrayList<Subject> listSubject = new ArrayList<>();
        if(allSubject.containsKey(term)){
            listSubject = allSubject.get(term);
        }

        return listSubject;
    }

    public String getAll(){

        String str = "";
        for(int i = 0; i < allSubject.size() ; i++){
            str += "Term " + (i+1) +"\n";
            for(int j = 0; j < allSubject.get(i+1).size(); j++){
                str += allSubject.get(i+1).get(j).getSubID() +
                        " " + allSubject.get(i+1).get(j).getSubName()  + " \n";
            }
        }


        return str;
    }

    public  ArrayList<Subject> getSubAll(){
        ArrayList<Subject> subAll = new ArrayList<>();
        for(int i = 0; i < allSubject.size() ; i++){
            for(int j = 0; j < allSubject.get(i+1).size(); j++){
                subAll.add(allSubject.get(i+1).get(j));
                //System.out.println(allSubject.get(i+1).get(j).getSubName());
            }
        }
        return subAll;

    }

    public String getDetail(int key,int index){

        String detail = "Subject Name : "+allSubject.get(key).get(index).getSubName()+"\n"+
                "Subject ID : "+allSubject.get(key).get(index).getSubID()+"\n"+
                "Credit : "+allSubject.get(key).get(index).getCredit()+"\n";


        return detail;
    }

    public String getNewDetails(String s){
        String str = "";
        for(int i = 0; i < allSubject.size() ; i++){
            for(int j = 0; j < allSubject.get(i+1).size(); j++){
                if(s == allSubject.get(i+1).get(j).getSubName()){
                    str +=  "ID : "+allSubject.get(i+1).get(j).getSubID() + " \nSubject : " + allSubject.get(i+1).get(j).getSubName()  + " \nCredit : "
                            + allSubject.get(i+1).get(j).getCredit() + " \nBasic Subject : "
                            + allSubject.get(i+1).get(j).getSubBasic() + "\nLevel : " +
                            allSubject.get(i+1).get(j).getLevel();
                    break;
                }
            }
        }
        return str;
    }


}
